﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Relationship;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataContext : DbContext
    {
        DbSet<Employee> employees;

        DbSet<Role> roles;

        DbSet<Customer> customers;

        DbSet<Preference> preferences;

        DbSet<PromoCode> promoCodes;

        DbSet<CustomerPreference> customerPreferences;


        public DataContext() : base()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .Property(e => e.FirstName).HasMaxLength(25).IsRequired();
            modelBuilder.Entity<Employee>()
                .Property(e => e.LastName).HasMaxLength(25).IsRequired();
            modelBuilder.Entity<Employee>()
                .Property(e => e.Email).HasMaxLength(100);
            modelBuilder.Entity<Employee>()
                .HasOne<Role>(e => e.Role);
            modelBuilder.Entity<Employee>()
                .HasKey(e => e.Id);

            modelBuilder.Entity<Role>()
                .Property(r => r.Description).HasMaxLength(500);
            modelBuilder.Entity<Role>()
                .Property(r => r.Name).HasMaxLength(50).IsRequired();
            modelBuilder.Entity<Role>()
                .HasKey(r => r.Id);

            modelBuilder.Entity<Customer>()
                .Property(c => c.Email).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<Customer>()
                .Property(c => c.FirstName).HasMaxLength(25).IsRequired();
            modelBuilder.Entity<Customer>()
                .Property(c => c.LastName).HasMaxLength(25).IsRequired();
            modelBuilder.Entity<Customer>()
                .HasMany(c => c.PromoCodes)
                .WithOne(p => p.Customer);
            modelBuilder.Entity<Customer>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Preference>()
                .Property(p => p.Name).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<Preference>()
                .HasKey(p => p.Id);

            modelBuilder.Entity<PromoCode>()
                .Property(p => p.PartnerName).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<PromoCode>()
                .Property(p => p.ServiceInfo).HasMaxLength(100).IsRequired();
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.PartnerManager);
            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Preference);
            modelBuilder.Entity<PromoCode>()
                .HasKey(p => p.Id);

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(p => new { p.CustomerId, p.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(p => p.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(fc => fc.CustomerId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(p => p.Preference)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(fc => fc.PreferenceId);

        }


    }
}
