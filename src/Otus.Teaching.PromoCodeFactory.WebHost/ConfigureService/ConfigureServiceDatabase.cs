﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.WebHost.ConfigureService
{
    public class ConfigureServiceDatabase
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            if (string.IsNullOrEmpty(Settings.Settings.Database.ConnectionString))
            {
                throw new NullReferenceException("Connection string does not empty");
            }

            services.AddDbContext<DataContext>(x =>
            {
                x.UseSqlite(Settings.Settings.Database.ConnectionString);
                x.UseLazyLoadingProxies();
            });
        }

    }
}
